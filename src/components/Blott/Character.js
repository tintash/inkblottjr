import React from 'react';
import { connect } from 'react-redux';
import { Image, Animated, Easing } from 'react-native';
import { setAnimatingFalse } from '../../actions';
import { CHAR_MOVE_TIME, SPRITE_UPDATE_INTERVAL } from '../../constants';
import Styles from '../Styles';

class Character extends React.Component {
    constructor(props) {
        super(props);

        this.charX = new Animated.Value(0);
        this.charY = new Animated.Value(0);

        this.moveX = null;
        this.moveY = null;

        this.state = {
            spriteIndex: 0,
            isAnimating: false,
            xOffset: 0,
            yOffset: 0
        }

        this.moveHorizontal = this.moveHorizontal.bind(this);
        this.moveVertical = this.moveVertical.bind(this);
        this.spriteUpdate = this.spriteUpdate.bind(this);
    }

    spriteUpdate() {
        if (this.props.isAnimating) {
            const numSprites = this.props.data.rows * this.props.data.columns;
            const spriteIndex = (this.state.spriteIndex + 1) % numSprites;

            const colNumber = spriteIndex % this.props.data.columns;
            const rowNumber = Math.floor(spriteIndex / this.props.data.columns);

            const xOffset = colNumber * this.props.width * -1;
            const yOffset = rowNumber * this.props.height * -1;

            this.setState({ spriteIndex, xOffset, yOffset });
        }
    }

    componentDidMount() {
        setInterval(this.spriteUpdate, SPRITE_UPDATE_INTERVAL);
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.isAnimating) {
            if (nextProps.isHorizontal) this.moveHorizontal(nextProps.nextXMove);
            else this.moveVertical(nextProps.nextYMove);
        }
    }

    moveHorizontal(value) {
        this.moveX = Animated.timing(
            this.charX,
            {
                toValue: value,
                easing: Easing.linear,
                duration: CHAR_MOVE_TIME
            },
        );
        this.moveX.start(() => this.props.setAnimatingFalse());
    }

    moveVertical(value) {
        this.moveY = Animated.timing(
            this.charY,
            {
                toValue: value,
                easing: Easing.linear,
                duration: CHAR_MOVE_TIME
            }
        );
        this.moveY.start(() => this.props.setAnimatingFalse());
    }

    render() {
        const { height, width, image, x, y, xFacing, yFacing, data } = this.props;

        return (
            <Animated.View
                style={[
                    Styles.imageContainer,
                    {
                        height: height,
                        width: width,
                        top: y - height,
                        left: x,
                        transform: [
                            { translateX: this.charX },
                            { translateY: this.charY },
                            { scaleX: xFacing },
                            { rotateZ: yFacing }
                        ],
                    },
                ]}
            >
                <Image
                    draggable={false}
                    style={[
                        Styles.image,
                        {
                            top: this.state.yOffset,
                            left: this.state.xOffset,
                            height: data.imageheight,
                            width: data.imagewidth,
                        }
                    ]}
                    source={image}
                    resizeMode='contain'
                />
            </Animated.View>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state.blott
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setAnimatingFalse: () => {
            dispatch(setAnimatingFalse())
        }
    }
}

Character = connect(mapStateToProps, mapDispatchToProps)(Character);

export default Character;