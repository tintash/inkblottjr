import React from 'react';
import { View, PanResponder } from 'react-native';
import { connect } from 'react-redux';
import { move } from '../../actions';
import { DIRECTIONS } from '../../constants';

class CharacterMovement extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentDirection: ''
        }

        this.move = this.move.bind(this);
    }

    move(currentDirection, touchPointX, touchPointY) {
        this.props.moveBlott(currentDirection);

        if(this.state.currentDirection !== '') {
            setTimeout(() => {
                this.getDirection(touchPointX, touchPointY);
            }, 100);
        }
    }

    componentWillMount() {
        let currentDirection;
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => !this.props.isModalVisible,

            onPanResponderGrant: (evt, gestureState) => {
                currentDirection = this.getDirection(evt.nativeEvent.locationX, evt.nativeEvent.locationY);
                this.setState({ currentDirection });
            },
            onPanResponderMove: (evt, gestureState) => {
                currentDirection = this.getDirection(evt.nativeEvent.locationX, evt.nativeEvent.locationY);
                this.setState({ currentDirection });
            },
            onPanResponderRelease: (evt, gestureState) => {
                this.setState({ currentDirection: '' })
            },
        });
    }

    getDirection(touchX, touchY) {
        let blottX, blottY, xRange, yRange;
        const { blott } = this.props;

        if (blott.yFacing === '0deg') {
            blottX = blott.x + blott.nextXMove + blott.width / 2;
            blottY = blott.y + blott.nextYMove - blott.height / 2;
            xRange = blott.width / 2;
            yRange = blott.height / 2;
        } else {
            blottX = blott.x + blott.nextXMove + blott.height / 2;
            blottY = blott.y + blott.nextYMove - blott.width / 2;
            xRange = blott.height / 2;
            yRange = blott.width / 2;
        }

        const diffX = touchX - blottX;
        const diffY = touchY - blottY;

        let direction = '';

        if (this.isHorizontal(diffX, diffY)) {
            if (diffX > xRange) direction = DIRECTIONS.RIGHT;
            else if (diffX < (xRange * -1)) direction = DIRECTIONS.LEFT;
        } else {
            if (diffY > yRange) direction = DIRECTIONS.DOWN;
            else if (diffY < (yRange * -1)) direction = DIRECTIONS.UP;
        }

        this.move(direction, touchX, touchY);
        return direction;
    }

    isHorizontal(diffX, diffY) {
        const dx = diffX > 0 ? diffX : diffX * -1;
        const dy = diffY > 0 ? diffY : diffY * -1;
        return dx > dy;
    }

    render() {
        const height = this.props.mapData.tileHeight * this.props.mapData.numRows;
        const width = this.props.mapData.tileWidth * this.props.mapData.numCols;

        return (
            <View
                style={{ height: height, width: width, position: 'absolute', elevation: 2 }}
                {...this._panResponder.panHandlers}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        mapData: state.mapData,
        isModalVisible: state.modalVisible,
        blott: state.blott
    }
}

const mapDispatchToProps = dispatch => {
    return {
        moveBlott: direction => {
            dispatch(move(direction));
        }
    }
}

CharacterMovement = connect(mapStateToProps, mapDispatchToProps)(CharacterMovement);

export default CharacterMovement;