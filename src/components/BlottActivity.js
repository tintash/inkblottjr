import React from 'react';
import { connect } from 'react-redux'
import { View } from 'react-native';
import { setGameState, loadPuzzle } from '../actions'
import GameScreen from './GameScreen';
import Styles from './Styles';

import mapData from '../assets/DemoLevel.json';
import puzzle from '../assets/DemoPuzzle.json';

class BlottActivity extends React.Component {
    componentDidMount() {
        this.props.setGameState(mapData);
        this.props.loadPuzzle(puzzle);
    }

    render() {
        return (
            <View style={Styles.mainContainer}>
                <GameScreen />
            </View>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setGameState: data => {
            dispatch(setGameState(data))
        },
        loadPuzzle: data => {
            dispatch(loadPuzzle(data))
        }
    }
}

BlottActivity = connect(
    null,
    mapDispatchToProps
)(BlottActivity);

export default BlottActivity;