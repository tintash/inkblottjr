import React from 'react';
import { View, Image } from 'react-native';

class Tile extends React.Component {
    shouldComponentUpdate(nextProps) {
        return this.props.tile !== nextProps.tile;
    }

    render() {
        const { tile, mapData } = this.props;
        return (
            <View style={getContainerStyle(mapData)}>
                <Image
                    style={getImageStyle(tile, mapData)}
                    source={tile.image}
                    resizeMode='cover'
                />
            </View>
        )
    }
}

const getContainerStyle = mapData => {
    return {
        overflow: 'hidden',
        height: mapData.tileHeight,
        width: mapData.tileWidth,
        backgroundColor: 'transparent',
    }
}

const getImageStyle = (tile, mapData) => {
    return {
        position: 'absolute',
        top: getOffsetY(tile, mapData),
        left: getOffsetX(tile, mapData),
        width: tile.imageData.imagewidth,
        height: tile.imageData.imageheight,
        backgroundColor: 'transparent',
    }

}

const getOffsetY = (tile, mapData) => {
    const tileIndex = tile.tileIndex - tile.indexOffset;
    return -1 * Math.floor(tileIndex / tile.imageData.columns) * mapData.tileHeight;
}

const getOffsetX = (tile, mapData) => {
    const tileIndex = tile.tileIndex - tile.indexOffset;
    return -1 * (tileIndex % tile.imageData.columns) * mapData.tileWidth;
}

export default Tile;