import { connect } from 'react-redux';
import Tile from './Tile';

const mapStateToProps = (state, ownProps) => {
    return {
        tile: state.tiles[ownProps.layerIndex][ownProps.tileIndex],
        mapData: state.mapData
    }
}

const MapTile = connect(mapStateToProps)(Tile);

export default MapTile;