import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import Styles from '../Styles';
import MapTile from './MapTile';

class MapLayer extends React.Component {
    constructor(props) {
        super(props);
        this.getTile = this.getTile.bind(this);
    }

    shouldComponentUpdate() {
        return false;
    }

    getTile(index) {
        return (
            <MapTile
                key={index}
                tileIndex={index}
                layerIndex={this.props.layerIndex}
            />
        );
    }

    render() {
        const { mapData, tiles } = this.props;
        const columns = mapData.numCols;

        return (
            <View
                pointerEvents={'box-none'}
                style={Styles.mapLayer}
            >
                {tiles.map((tileData, index) => {
                    if ((index % columns) === 0) {
                        const rowTiles = tiles.slice(index, index + columns);
                        return (
                            <View key={index} style={Styles.mapRow}>
                                {rowTiles.map((_, rowIndex) => this.getTile(index + rowIndex))}
                            </View>
                        );
                    }
                    return;
                })}
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        tiles: state.tiles[ownProps.layerIndex],
        mapData: state.mapData
    }
}

MapLayer = connect(mapStateToProps)(MapLayer);

export default MapLayer;