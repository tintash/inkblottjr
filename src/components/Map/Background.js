import React from 'react';
import { View, Image } from 'react-native';
import { connect } from 'react-redux';
import Styles from '../Styles';
import DataMap from '../../assets/DataMap';

class Background extends React.Component {
    constructor(props) {
        super(props);
        this.getTile = this.getTile.bind(this);
    }

    shouldComponentUpdate(nextProps) {
        return this.props.numTiles === 0;
    }

    getTile(index) {
        return (
            <View
                key={index}
                style={[
                    {
                        overflow: 'hidden',
                        height: this.props.mapData.tileHeight,
                        width: this.props.mapData.tileWidth,
                        backgroundColor: 'transparent',
                    },
                ]}
            >
                <Image
                    style={[
                        Styles.image,
                        {
                            width: this.props.mapData.tileWidth,
                            height: this.props.mapData.tileHeight,
                        }
                    ]}
                    source={DataMap.images.backgroundTile}
                    resizeMode='cover'
                />
            </View>
        );
    }

    render() {
        const { mapData } = this.props;
        const columns = mapData.numCols;
        const tiles = Array(this.props.numTiles).fill(0);
        return (
            <View
                pointerEvents={'none'}
                style={Styles.backgroundLayer}
            >
                {tiles.map((tileData, index) => {
                    if ((index % columns) === 0) {
                        const rowTiles = tiles.slice(index, index + columns);
                        return (
                            <View key={index} style={Styles.mapRow}>
                                {rowTiles.map((_, rowIndex) => this.getTile(index + rowIndex))}
                            </View>
                        );
                    }
                    return;
                })}
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        mapData: state.mapData,
        numTiles: state.tiles[0] ? state.tiles[0].length : 0
    }
}

Background = connect(mapStateToProps)(Background);

export default Background;