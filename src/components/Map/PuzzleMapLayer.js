import React from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import Styles from '../Styles';
import Tile from './Tile';

class PuzzleMapLayer extends React.Component {
    constructor(props) {
        super(props);
        this.getTile = this.getTile.bind(this);
    }

    shouldComponentUpdate() {
        return !this.props.tiles.length;
    }

    getOffsetY(tileIndex, imageData) {
        return -1 * Math.floor(tileIndex / imageData.columns) * 64;
    }

    getOffsetX(tileIndex, imageData) {
        return -1 * (tileIndex % imageData.columns) * 64;
    }

    getTile(index) {
        const tile = this.props.tiles[index];

        return (
            <Tile key={index} tile={tile} mapData={this.props.mapData} />
        );
    }

    render() {
        const { mapData, tiles } = this.props;
        const columns = mapData.numCols;

        let textX, textY, puzzleTiles;
        puzzleTiles = tiles.filter(tile => tile.tileType === 'PuzzleTile01');
        const startIndex = tiles.indexOf(puzzleTiles[0]);

        textX = (startIndex % columns) * mapData.tileWidth;
        textY = Math.floor(startIndex / columns) * mapData.tileHeight;

        return (
            <View
                pointerEvents={'box-none'}
                style={Styles.mapLayer}
            >
                {tiles.map((tileData, index) => {
                    if ((index % columns) === 0) {
                        const rowTiles = tiles.slice(index, index + columns);
                        return (
                            <View key={index} style={Styles.mapRow}>
                                {rowTiles.map((_, rowIndex) => this.getTile(index + rowIndex))}
                            </View>
                        );
                    }
                    return;
                })}
                <View style={[
                    Styles.puzzleSentenceContainer,
                    {
                        width: mapData.tileWidth * puzzleTiles.length,
                        height: mapData.tileHeight,
                        top: textY,
                        left: textX
                    }
                ]}>
                    <Text style={Styles.puzzleSentence} selectable={false}>{this.props.puzzleSentence}</Text>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        tiles: state.puzzleTiles,
        puzzleSentence: state.puzzleSentence,
        mapData: state.mapData
    }
}

PuzzleMapLayer = connect(mapStateToProps)(PuzzleMapLayer);

export default PuzzleMapLayer;