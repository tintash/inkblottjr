import React from 'react';
import { View, ImageBackground, Animated, PanResponder, Text, Platform } from 'react-native';
import Styles from '../Styles';

class Affix extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pan: new Animated.ValueXY(),
            affixX: 0,
            affixY: 0,
            placeHolder: []
        }

        this.childReference = null;
        this.parentReference = null;
        this.checkPlaceHolder = this.checkPlaceHolder.bind(this);
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => this.props.enable,
            onPanResponderMove: (event, gestureState) => {
                Animated.event(
                    [
                        null,
                        {
                            dx: this.state.pan.x,
                            dy: this.state.pan.y,
                        },
                    ],
                )(event, gestureState);
            },
            onPanResponderRelease: (event, gesture) => {
                Animated.spring(this.state.pan, {
                    toValue: { x: 0, y: 0 },
                }).start();
                this.checkPlaceHolder(gesture);
            }
        });
    }

    checkPlaceHolder(gesture) {
        this.props.affixPlaceHolders.forEach(placeHolder => {
            const { width, height, px, py } = placeHolder.measurement;

            const distX = px - this.state.affixX;
            const distY = py - this.state.affixY;

            if (Platform.OS === 'web') {
                if (gesture.dx > distX && gesture.dx < distX + width
                    && gesture.dy > distY - height / 2 && gesture.dx < distY + height / 2) {
                    this.props.setAffixString(this.props.affix.id, placeHolder.text);
                    this.props.setPlaceHolderText(placeHolder.id, this.props.affix.string);
                    return;
                }
            } else {
                if (gesture.dx > distX && gesture.dx < distX + width
                    && gesture.dy > distY - height && gesture.dx < distY) {
                    this.props.setAffixString(this.props.affix.id, placeHolder.text);
                    this.props.setPlaceHolderText(placeHolder.id, this.props.affix.string);
                    return;
                }
            }
        })
    }

    render() {
        const { affix, scale, top, left } = this.props;
        return (
            <View
                ref={view => this.parentReference = view}
                pointerEvents={'box-none'}
                style={[
                    Styles.screenContainer,
                    {
                        position: 'absolute',
                        top: top,
                        left: left,
                        transform: [
                            { scale: scale }
                        ]
                    }
                ]}
            >
                <Animated.View
                    ref={view => this.childReference = view}
                    onLayout={() => {
                        this.childReference.getNode().measure((fx, fy, width, height, px, py) => {
                            this.setState({ affixX: px, affixY: py })
                        })
                    }}
                    collapsable={false}
                    {...this._panResponder.panHandlers}
                    pointerEvents={'box-none'}
                    style={[
                        Styles.imageContainer,
                        {
                            height: affix.height,
                            width: affix.width,
                            top: affix.y - affix.height,
                            left: affix.x,
                            transform: [
                                { translateX: this.state.pan.x },
                                { translateY: this.state.pan.y },
                            ]
                        },
                    ]}
                >
                    <ImageBackground
                        style={[
                            Styles.image,
                            {
                                height: affix.data.imageheight,
                                width: affix.data.imagewidth,
                            }
                        ]}
                        source={affix.image}
                        resizeMode='contain'
                        draggable={false}
                    >
                        <View style={Styles.affixContainer}>
                            <Text style={Styles.affixText}>{affix.string}</Text>
                        </View>
                    </ImageBackground>
                </Animated.View>
            </View>
        );
    }
}

export default Affix;