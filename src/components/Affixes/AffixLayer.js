import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import Affix from './Affix';
import { setPlaceHolderMeasure, setPlaceHolderText, setAffixString } from '../../actions';

class AffixLayer extends React.Component {
    render() {
        const { affixes } = this.props;
        return (
            <View>
                {affixes.map((affix, index) =>
                    <Affix
                        key={index}
                        affix={affix}
                        enable={this.props.touchEnable}
                        scale={this.props.scale}
                        top={this.props.top}
                        left={this.props.left}
                        affixPlaceHolders={this.props.affixPlaceHolders}
                        setPlaceHolderMeasure={this.props.setPlaceHolderMeasure}
                        setPlaceHolderText={this.props.setPlaceHolderText}
                        setAffixString={this.props.setAffixString}
                        />
                )}
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        affixes: state.affixes,
        touchEnable: state.modalVisible,
        affixPlaceHolders: state.puzzleWord.filter(part => part.type === 'affix')
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setPlaceHolderMeasure: (id, data) => {
            dispatch(setPlaceHolderMeasure(id, data))
        },
        setPlaceHolderText: (id, text) => {
            dispatch(setPlaceHolderText(id, text))
        },
        setAffixString: (id, string) => {
            dispatch(setAffixString(id, string))
        }
    }
}

AffixLayer = connect(mapStateToProps, mapDispatchToProps)(AffixLayer);

export default AffixLayer;