import { Dimensions, StyleSheet } from 'react-native';
import { MAX_SCREEN_HEIGHT, MAX_SCREEN_WIDTH } from '../constants';

const { width } = Dimensions.get('window');

const Styles = StyleSheet.create({
    homeContainer: {
        height: '100%',
        paddingVertical: 0,
        flex: 1,
        backgroundColor: '#062d27',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    homeButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    homeButtonText: {
        color: '#fff',
        fontWeight: '500',
        fontSize: width / 50
    },
    title: {
        fontSize: width / 10,
        fontWeight: '600'
    },
    homeButton: {
        width: width / 10,
        backgroundColor: '#000',
        paddingTop: width / 75,
        paddingBottom: width / 75,
        alignItems: 'center',
        borderRadius: 5,
    },
    mainContainer: {
        backgroundColor: '#062d27',
        flex: 1,
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },
    mapContainer: {
        backgroundColor: '#000',
        overflow: 'hidden',
        maxWidth: MAX_SCREEN_WIDTH,
        maxHeight: MAX_SCREEN_HEIGHT,
        height: '100%',
        width: '100%'
    },
    mapSubContainer: {
        position: 'absolute',
    },
    mapRow: {
        overflow: 'visible',
        flexDirection: 'row',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center'
    },
    modalWindow: {
        backgroundColor: 'transparent',
        justifyContent: 'center',
    },
    affixContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    affixText: {
        position: 'absolute',
        fontFamily: 'nyala',
        fontSize: 21
    },
    backgroundLayer: {
        position: 'absolute',
        top: 0
    },
    wordStoneText: {
        fontSize: 35,
        fontFamily: 'nyala'
    },
    mapLayer: {
        position: 'absolute',
        top: 0
    },
    puzzleSentence: {
        fontFamily: 'nyala',
        fontSize: 21
    },
    puzzleSentenceContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        position: 'absolute'
    },
    modalPuzzleText: {
        fontFamily: 'nyala',
        fontSize: 50
    },
    modalStaticTextContainer: {
        position: 'relative',
        top: -3,
        margin: 20
    },
    modalAffixTextContainer: {
        width: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        position: 'relative',
        top: -5
    },
    modalInfoText: {
        fontFamily: 'nyala',
        fontSize: 24
    },
    modalInfoTextContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },
    screenContainer: {
        height: MAX_SCREEN_HEIGHT,
        width: MAX_SCREEN_WIDTH
    },
    modalPuzzle: {
        height: MAX_SCREEN_HEIGHT,
        width: MAX_SCREEN_WIDTH,
        position: 'absolute',
        flex: 1,
        top: -160,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    wordStoneContainer: {
        backgroundColor: 'transparent',
        borderColor: '#eee',
        borderRadius: 3,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 2,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        position: 'absolute'
    },
    imageContainer: {
        position: 'absolute',
        backgroundColor: 'transparent',
        overflow: 'hidden'
    },
    image : {
        position: 'absolute',
        backgroundColor: 'transparent'
    }
})

export default Styles;