import React from 'react';
import { connect } from 'react-redux';
import { View, Animated, Easing } from 'react-native';
import Styles from './Styles';
import MapLayer from './Map/MapLayer';
import MapDecorationLayer from './Decorations/MapDecorationLayer';
import Character from './Blott/Character';
import CharacterMovement from './Blott/CharacterMovement';
import WordStone from './WordStone/WordStone';
import HudLayer from './HUD/HudLayer';
import AffixLayer from './Affixes/AffixLayer';
import Background from './Map/Background';
import Crystal from './Decorations/Crystal';
import { setMapScale } from './../actions';
import { MAX_SCREEN_WIDTH, MAX_SCREEN_HEIGHT, MAP_SCROLL_TIME } from '../constants';
import ModalLayer from './Popup/ModalLayer';
import PuzzleMapLayer from './Map/PuzzleMapLayer';

class GameScreen extends React.Component {
    constructor(props) {
        super(props);

        this.mapScroll = new Animated.Value(0);
        this.state = {
            scale: 1,
            screenWidth: MAX_SCREEN_WIDTH,
            screenHeight: MAX_SCREEN_HEIGHT
        }

        this.setScale = this.setScale.bind(this);
        this.scrollAnimation = this.scrollAnimation.bind(this);
        this.getOriginOffsetX = this.getOriginOffsetX.bind(this);
        this.getOriginOffsetY = this.getOriginOffsetY.bind(this);
    }

    scrollAnimation = value => {
        Animated.timing(
            this.mapScroll,
            {
                toValue: value,
                easing: Easing.linear,
                duration: MAP_SCROLL_TIME

            }
        ).start();
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.scale !== this.state.scale
            || this.props.modalVisible !== nextProps.modalVisible
            || !this.props.tiles.length;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mapData.scrollOffset !== this.props.mapData.scrollOffset) {
            this.scrollAnimation(nextProps.mapData.scrollOffset);
        }
    }

    setScale(screenWidth, screenHeight) {
        const scale = screenWidth / (this.props.mapData.numCols * this.props.mapData.tileWidth);
        this.setState({ scale, screenHeight, screenWidth });
        this.props.setMapScale(scale);
    }

    getOriginOffsetX() {
        const { scale } = this.state;
        return (MAX_SCREEN_WIDTH - (scale * MAX_SCREEN_WIDTH)) / -2;
    }

    getOriginOffsetY() {
        const { scale } = this.state;
        return (MAX_SCREEN_HEIGHT - (scale * MAX_SCREEN_HEIGHT)) / -2;
    }

    render() {
        const { tiles } = this.props;
        const { scale, screenHeight, screenWidth } = this.state;

        return (
            <View
                style={Styles.mapContainer}
                pointerEvents={'box-none'}
                onLayout={(event) => {
                    const { width, height } = event.nativeEvent.layout;
                    this.setScale(width, height);
                }}
            >
                <View
                    pointerEvents={'box-none'}
                    style={[
                        {
                            top: this.getOriginOffsetY(),
                            left: this.getOriginOffsetX(),
                            transform: [
                                { scale: scale }
                            ]
                        },
                        Styles.mapSubContainer
                    ]}
                >
                    <Animated.View
                        style={{
                            top: this.mapScroll
                        }}
                    >
                        <Background />
                        {
                            tiles.map((layerTiles, index) => {
                                switch (layerTiles[0].type) {
                                    case 'tile':
                                        return <MapLayer
                                            key={index}
                                            layerIndex={index} />
                                    case 'decoration':
                                        return <MapDecorationLayer key={index} layerIndex={index} />
                                    default:
                                        return;
                                }
                            })
                        }
                        <PuzzleMapLayer />
                        <Crystal />
                        <CharacterMovement />
                        <WordStone />
                        <Character />
                    </Animated.View>
                    <ModalLayer scale={scale} height={screenHeight} width={screenWidth} />
                    <HudLayer />
                </View>
                <AffixLayer scale={scale} top={this.getOriginOffsetY()} left={this.getOriginOffsetX()} />
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        mapData: state.mapData,
        tiles: state.tiles,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setMapScale: scale => {
            dispatch(setMapScale(scale));
        },
    }
}

GameScreen = connect(mapStateToProps, mapDispatchToProps)(GameScreen);

export default GameScreen;