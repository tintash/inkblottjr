import React from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { setModalVisible, setTileType } from '../../actions';
import Styles from '../Styles';
import WordStoneTile from './WordStoneTile';

class WordStone extends React.Component {
    constructor(props) {
        super(props);

        this.handlePress = this.handlePress.bind(this);
    }

    setTileType(decos, map) {
        decos.forEach(deco => {
            const index = (Math.floor(deco.y / map.tileHeight) - 1) * map.numCols + (Math.floor(deco.x / map.tileWidth));
            this.props.setTileType(index, 'WordStone');
        })
    }

    shouldComponentUpdate(nextProps) {
        return this.props.stoneTouchable !== nextProps.stoneTouchable
            || this.props.puzzleWord !== nextProps.puzzleWord
            || !this.props.stones.length;
    }

    componentWillReceiveProps(nextProps) {
        this.setTileType(nextProps.stones, nextProps.mapData);
    }

    handlePress() {
        this.props.setModalVisible();
    }

    render() {
        const { stones, stoneTouchable, puzzleWord } = this.props;
        return (
            <View
                style={[Styles.screenContainer, { position: 'absolute' }]}
                pointerEvents={stoneTouchable ? 'box-none' : 'none'}
            >
                {stones.map((stone, index) => <WordStoneTile key={index} deco={stone} />)}
                {!!stones.length &&
                    <TouchableWithoutFeedback onPress={this.handlePress} disabled={!stoneTouchable}>
                        <View
                            pointerEvents={'auto'}
                            style={[
                                Styles.wordStoneContainer,
                                {
                                    borderWidth: stoneTouchable ? 2 : 0,
                                    shadowOpacity: stoneTouchable ? 0.8 : 0,
                                    elevation: stoneTouchable ? 5 : 0,
                                    width: stones[0].width * stones.length,
                                    height: stones[0].height,
                                    top: stones[0].y - stones[0].height,
                                    left: stones[0].x,
                                }
                            ]}>
                            <Text style={Styles.wordStoneText} selectable={false}>
                                {puzzleWord.map(part => part.text)}
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                }
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        stones: state.wordStone,
        mapData: state.mapData,
        stoneTouchable: state.stoneTouchable,
        puzzleWord: state.puzzleWord
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setTileType: (index, tileType) => {
            dispatch(setTileType(index, tileType))
        },
        setModalVisible: () => {
            dispatch(setModalVisible())
        }
    }
}

WordStone = connect(mapStateToProps, mapDispatchToProps)(WordStone);

export default WordStone;