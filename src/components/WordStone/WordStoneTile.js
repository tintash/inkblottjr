import React from 'react';
import { View, Image } from 'react-native';
import Styles from '../Styles';

class WordStoneTile extends React.Component {
    constructor(props) {
        super(props);

        this.getOffsetX = this.getOffsetX.bind(this);
        this.getOffsetY = this.getOffsetY.bind(this);
    }

    getOffsetY(tileIndex) {
        return -1 * Math.floor(tileIndex / (this.props.deco.data.imagewidth / this.props.deco.width)) * this.props.deco.width;
    }

    getOffsetX(tileIndex) {
        return -1 * (tileIndex % (this.props.deco.data.imagewidth / this.props.deco.width)) * this.props.deco.width;
    }

    render() {
        const { height, width, image, x, y, data, gid, indexOffset } = this.props.deco;
        const tileIndex = gid - indexOffset;
        return (
            <View
                style={[
                    Styles.imageContainer,
                    {
                        height: height,
                        width: width,
                        top: y - height,
                        left: x,
                    }
                ]}
            >
                <Image
                    draggable={false}
                    style={[
                        Styles.image,
                        {
                            top: this.getOffsetY(tileIndex),
                            left: this.getOffsetX(tileIndex),
                            height: data.imageheight,
                            width: data.imagewidth,
                        }
                    ]}
                    source={image}
                    resizeMode='contain'
                />
            </View>
        );
    }
}

export default WordStoneTile;