import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Styles from './Styles';

class HomeScreen extends React.Component {
    render() {
        return (
            <View style={Styles.homeContainer}>
                <Text style={Styles.title}>Aqua</Text>
                <View style={Styles.homeButtonContainer}>
                    <TouchableOpacity style={Styles.homeButton} onPress={Actions.Game}>
                        <Text style={Styles.homeButtonText}>Sample</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default HomeScreen;