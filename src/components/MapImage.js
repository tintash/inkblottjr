import React from 'react';
import { View, ImageBackground } from 'react-native';
import Styles from './Styles';

class MapImage extends React.Component {
    render() {
        const { height, width, x, y, data, image } = this.props.item;
        return (
            <View
                ref={view => { if (this.props.setRef) this.props.setRef(view) }}
                pointerEvents={'box-none'}
                style={[
                    Styles.imageContainer,
                    {
                        height: height,
                        width: width,
                        top: y - height,
                        left: x,
                    }
                ]}
            >
                <ImageBackground
                    style={[
                        Styles.image,
                        {
                            height: data.imageheight,
                            width: data.imagewidth,
                        }
                    ]}
                    source={image}
                    resizeMode='contain'
                    draggable={false}
                >
                    {this.props.children}
                </ImageBackground>
            </View>
        )
    }
}

export default MapImage;