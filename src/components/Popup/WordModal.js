import React from 'react';
import { View, Text } from 'react-native';
import Modal from 'react-native-modalbox';
import { connect } from 'react-redux';
import ModalInterface from './ModalInterface';
import Styles from '../Styles';
import Puzzle from './Puzzle';
import ModalButton from './ModalButton';
import ModalImage from './ModalImage';
import {
    setModalClose,
    setPlaceHolderMeasure,
    checkPuzzleSolved
} from '../../actions';

class WordModal extends React.Component {
    constructor(props) {
        super(props);
        this.placeHolder = null;
        this.onModalClose = this.onModalClose.bind(this);
    }

    onModalClose() {
        this.props.checkPuzzleSolved();
    }

    handleHelp() {
        // handle 'Help Button' press
    }

    render() {
        const { background, infoStone, helpButton, doneButton, blott } = ModalInterface;
        return (
            <View style={Styles.modalContainer} pointerEvents={'box-none'}>
                <Modal
                    style={Styles.modalWindow}
                    entry={'top'}
                    isOpen={this.props.modalVisible}
                    onClosed={this.onModalClose}
                    backdrop={true}
                    backdropPressToClose={false}
                    swipeToClose={false}
                >
                    <ModalImage image={background}>
                        <ModalButton button={helpButton} onPress={this.handleHelp} />
                        <Puzzle word={this.props.puzzleWord} setPlaceHolderMeasure={this.props.setPlaceHolderMeasure} />
                        <ModalImage image={infoStone}>
                            <View style={Styles.modalInfoTextContainer}>
                                <Text style={Styles.modalInfoText}>{this.props.puzzleSentence}</Text>
                            </View>
                        </ModalImage>
                        <ModalImage image={blott} />
                        <ModalButton button={doneButton} onPress={this.props.setModalClose} />
                    </ModalImage>
                </Modal>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        modalVisible: state.modalVisible,
        mapData: state.mapData,
        puzzleSentence: state.puzzleSentence,
        puzzleWord: state.puzzleWord
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setModalClose: () => {
            dispatch(setModalClose());
        },
        checkPuzzleSolved: () => {
            dispatch(checkPuzzleSolved());
        },
        setPlaceHolderMeasure: (id, measure) => {
            dispatch(setPlaceHolderMeasure(id, measure));
        }
    }
}

WordModal = connect(mapStateToProps, mapDispatchToProps)(WordModal);

export default WordModal;