import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import Styles from '../Styles';
import ModalInterface from './ModalInterface';
import ModalImage from './ModalImage';

const screenHeight = Dimensions.get('window').height;

class Puzzle extends React.Component {
    constructor(props) {
        super(props);

        this.reference = null;
    }

    render() {
        const { innerWordShort } = ModalInterface;
        const { word } = this.props;

        return (
            <View
                style={Styles.modalPuzzle}
                pointerEvents={'none'}
            >
                {word.map((part, index) =>
                    part.type === 'affix' ?
                        <ModalImage key={index} image={innerWordShort}>
                            <View
                                ref={view => this.reference = view}
                                onLayout={() => {
                                    this.reference.measure((fx, fy, width, height, px, py) => {
                                        if (py < 0) {
                                            py = screenHeight + py;
                                        }

                                        this.props.setPlaceHolderMeasure(part.id, { px, py, width, height });
                                    })
                                }}
                                style={Styles.modalAffixTextContainer}>
                                <Text style={Styles.modalPuzzleText}>{part.text}</Text>
                            </View>
                        </ModalImage> :
                        <View key={index} style={Styles.modalStaticTextContainer}>
                            <Text style={Styles.modalPuzzleText}>{part.text}</Text>
                        </View>
                )}
            </View>
        )
    }
}

export default Puzzle;