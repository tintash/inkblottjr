import React from 'react';
import { TouchableWithoutFeedback, Image } from 'react-native';

const ModalButton = ({ button, onPress }) =>
    <TouchableWithoutFeedback onPress={onPress}>
        <Image
            draggable={false}
            source={button.source}
            style={{
                height: button.height,
                width: button.width,
                position: 'relative',
                left: button.xOffset,
                top: button.yOffset
            }} />
    </TouchableWithoutFeedback>

export default ModalButton;