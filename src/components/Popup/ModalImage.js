import React from 'react';
import { ImageBackground } from 'react-native';

class ModalImage extends React.Component {
    render() {
        return (
            <ImageBackground
                draggable={false}
                source={this.props.image.source}
                resizeMode={'contain'}
                style={{
                    height: this.props.image.height,
                    width: this.props.image.width,
                    position: 'relative',
                    alignItems:'center',
                    left: this.props.image.xOffset,
                    top: this.props.image.yOffset
                }}>
                {this.props.children}
            </ImageBackground>
        )
    }
}

export default ModalImage;
