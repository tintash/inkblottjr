import DataMap from '../../assets/DataMap';

const ModalInterface = {
    background: {
        source: DataMap.images.BG,
        height: 351,
        width: 1024,
        xOffset: 0,
        yOffset: 0
    },
    blott: {
        source: DataMap.images.Blott,
        height: 111,
        width: 103,
        xOffset: -405,
        yOffset: 30
    },
    helpButton: {
        source: DataMap.images.HelpButton,
        height: 51,
        width: 52,
        xOffset: 480,
        yOffset: 10
    },
    doneButton: {
        source: DataMap.images.DoneButton,
        height: 77,
        width: 155,
        xOffset: 425,
        yOffset: -45
    },
    infoStone: {
        source:DataMap.images.InfoStone,
        height: 124,
        width: 670,
        xOffset: 0,
        yOffset: 215
    },
    innerWordShort: {
        source: DataMap.images.InnerWordsBG_short,
        height: 95,
        width: 139,
        xOffset: 0,
        yOffset: 0
    }
}

export default ModalInterface;