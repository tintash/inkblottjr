import React from 'react';
import { View } from 'react-native';
import WordModal from './WordModal';

class ModalLayer extends React.Component {
    render() {
        const { height, width, scale } = this.props;
        return (
            <View
                pointerEvents={'box-none'}
                style={{ height: height / scale, width: width / scale, position: 'absolute' }}
            >
                <WordModal />
            </View>
        )
    }
}

export default ModalLayer;