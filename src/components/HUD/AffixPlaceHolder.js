import React from 'react';
import { View, Image } from 'react-native';
import Styles from '../Styles';

class AffixPlaceHolder extends React.Component {
    render() {
        const { height, width, image, x, y, data } = this.props.deco;
        return (
            <View
                style={[
                    Styles.imageContainer,
                    {
                        height: height,
                        width: width,
                        top: y - height,
                        left: x,
                    }
                ]}
            >
                <Image
                    draggable={false}
                    style={[
                        Styles.image,
                        {
                            height: data.imageheight,
                            width: data.imagewidth,
                        }
                    ]}
                    source={image}
                    resizeMode='contain'
                />
            </View>
        );
    }
}

export default AffixPlaceHolder;