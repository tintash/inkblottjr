import React from 'react';
import { View, Image } from 'react-native';
import Styles from '../Styles';

class CrystalCollector extends React.Component {
    constructor(props) {
        super(props);

        this.reference = null;
    }

    render() {
        const { height, width, image, x, y, data } = this.props.deco;
        return (
            <View
                ref={view => this.reference = view}
                onLayout={() => {
                    this.reference.measure((fx, fy, width, height, px, py) =>
                        this.props.setPlaceholder({ x: px + width / 2, y: py - height / 2 })
                    )
                }}
                style={[
                    Styles.imageContainer,
                    {
                        height: height,
                        width: width,
                        top: y - height,
                        left: x,
                    }
                ]}
            >
                <Image
                    draggable={false}
                    style={[
                        Styles.image,
                        {
                            height: data.imageheight,
                            width: data.imagewidth,
                        }
                    ]}
                    source={image}
                    resizeMode='contain'
                />
            </View>
        );
    }
}

export default CrystalCollector;