import React from 'react';
import { View, Image, TouchableWithoutFeedback } from 'react-native';
import Styles from '../Styles';

class PauseButton extends React.Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        // Handle pause event
    }

    render() {
        const { height, width, image, x, y, data } = this.props.deco;
        return (
            <TouchableWithoutFeedback onPress={this.handleClick}>
                <View
                    style={[
                        Styles.imageContainer,
                        {
                            height: height,
                            width: width,
                            top: y - height,
                            left: x,
                        }
                    ]}
                >
                    <Image
                        draggable={false}
                        style={[
                            Styles.image,
                            {
                                height: data.imageheight,
                                width: data.imagewidth,
                            }
                        ]}
                        source={image}
                        resizeMode='contain'
                    />
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

export default PauseButton;