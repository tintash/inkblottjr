import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import CrystalCollector from './CrystalCollector';
import PauseButton from './PauseButton';
import AffixPlaceHolder from './AffixPlaceHolder';
import Styles from '../Styles';
import { setCrystalPlaceholder } from '../../actions';

class HudLayer extends React.Component {
    render() {
        const { hud, setPlaceholder } = this.props;
        return (
            <View style={[Styles.screenContainer, { position: 'absolute' }]} pointerEvents={'box-none'}>
                {hud[0] && <CrystalCollector setPlaceholder={setPlaceholder} deco={hud[0]} />}
                {hud[2] && <AffixPlaceHolder deco={hud[2]} />}
                {hud[1] && <PauseButton deco={hud[1]} />}
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        hud: state.hud,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setPlaceholder: position => {
            dispatch(setCrystalPlaceholder(position))
        }
    }
}

HudLayer = connect(mapStateToProps, mapDispatchToProps)(HudLayer);

export default HudLayer;