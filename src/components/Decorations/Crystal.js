import React from 'react';
import { View, Animated, ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import Styles from '../Styles';

class Crystal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            pan: new Animated.ValueXY(),
            scale: new Animated.Value(1),
            position: { x: 0, y: 0 },
            movement: { x: 0, y: 0 }
        }

        this.reference = null;
        this.animateCrystal = this.animateCrystal.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.crystalPlaceholder === null && nextProps.crystalPlaceholder !== null) {
            this.setState({ movement: {
                x: nextProps.crystalPlaceholder.x - this.state.position.x,
                y: nextProps.crystalPlaceholder.y - this.state.position.y
            }})
        }

        if(nextProps.isCollected && !this.props.isCollected) {
            this.animateCrystal();
        }
    }

    animateCrystal() {
        Animated.parallel([
            Animated.timing(
                this.state.pan,
                {
                    toValue: { x: this.state.movement.x, y: this.state.movement.y - this.props.mapData.scrollOffset },
                    duration: 1000
                }
            ),
            Animated.timing(
                this.state.scale,
                {
                    toValue: 0.5,
                    duration: 1000
                }
            )
        ]).start(() => this.setState({ scale: 0 }));
    }

    render() {
        const { x, y, height, width, data, image } = this.props.crystal;

        return (
            <View style={Styles.screenContainer}>
                <Animated.View
                    ref={view => this.reference = view}
                    onLayout={() => {
                        this.reference.getNode().measure((fx, fy, width, height, px, py) =>
                            this.setState({ position: { x: px, y: py } })
                        )
                    }}
                    pointerEvents={'box-none'}
                    style={[
                        Styles.imageContainer,
                        {
                            height: height,
                            width: width,
                            top: y - height,
                            left: x,
                            transform: [
                                { translateX: this.state.pan.x },
                                { translateY: this.state.pan.y },
                                { scale: this.state.scale }
                            ]
                        }
                    ]}
                >
                    <ImageBackground
                        style={[
                            Styles.image,
                            {
                                height: data.imageheight,
                                width: data.imagewidth,
                            }
                        ]}
                        source={image}
                        resizeMode='contain'
                        draggable={false}
                    />
                </Animated.View>
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        crystal: state.crystal,
        isCollected: state.crystalCollected,
        mapData: state.mapData,
        crystalPlaceholder: state.crystalPlaceholder
    }
}

Crystal = connect(mapStateToProps)(Crystal);

export default Crystal;