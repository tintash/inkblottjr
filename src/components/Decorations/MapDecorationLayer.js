import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import MapImage from '../MapImage';
import Styles from '../Styles';

class MapDecorationLayer extends React.Component {
    render() {
        const { decos } = this.props;
        return (
            <View style={[Styles.screenContainer, { position: 'absolute' }]} pointerEvents={'none'}>
                {decos.map((deco, index) => <MapImage key={index} item={deco} />)}
            </View>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        decos: state.tiles[ownProps.layerIndex],
    }
}

MapDecorationLayer = connect(mapStateToProps)(MapDecorationLayer);

export default MapDecorationLayer;