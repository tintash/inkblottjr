import React from 'react'
import { Provider } from 'react-redux'
import { Scene, Router } from 'react-native-router-flux'
import { createStore, applyMiddleware, compose } from 'redux'
import { reduxBatch } from '@manaflair/redux-batch';
import thunk from 'redux-thunk';
import rootReducer from './reducers'
import HomeScreen from './components/HomeScreen';
import BlottActivity from './components/BlottActivity';

const store = createStore(
  rootReducer,
  compose(reduxBatch, applyMiddleware(thunk), reduxBatch)
);

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Scene key='root' hideNavBar>
            <Scene
              key='Home'
              component={HomeScreen}
              type='replace'
              initial />
            <Scene
              key='Game'
              component={BlottActivity}
              type='replace' />
          </Scene>
        </Router>
      </Provider>
    )
  }
}

export default App;