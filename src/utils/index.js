import DataMap from '../assets/DataMap';
import { Dimensions } from 'react-native';
import { MAX_SCREEN_HEIGHT } from '../constants';
const screenHeight = Dimensions.get('window').height;

export const isTilePassable = (index, tiles, puzzleTiles) => {
    if (index < 0 || index >= tiles[0].length) return false;
    if (tiles[0][index].tileType.includes('StoneTile')) return false;
    if (tiles[0][index].tileType.includes('WordStone')) return false;
    if (tiles[0][index].tileType.includes('CorruptTile')) return false;
    if (tiles[0][index].tileType.includes('GrassStone')) return false;
    if (puzzleTiles[index].tileType === 'PuzzleTile01') return false;
    return true;
}

export const isWordStone = (index, tiles, columns) => {
    if (tiles[0][index] !== undefined && tiles[0][index].tileType === 'WordStone') return true;
    //left
    if (tiles[0][index - 1] !== undefined && tiles[0][index - 1].tileType === 'WordStone') return true;
    //right
    if (tiles[0][index + 1] !== undefined && tiles[0][index + 1].tileType === 'WordStone') return true;
    //up
    if (tiles[0][index - columns] !== undefined && tiles[0][index - columns].tileType === 'WordStone') return true;
    //down
    if (tiles[0][index + columns] !== undefined && tiles[0][index + columns].tileType === 'WordStone') return true;
    return false;
}

export const hasReachedCrytsal = (crystal, nextIndex, map) => {
    const crystalLowestIndex = (Math.floor(crystal.y / map.tileHeight)) * map.numCols + (Math.floor(crystal.x / map.tileWidth));
    const crystalHighestIndex = (Math.floor((crystal.y - crystal.height) / map.tileHeight)) * map.numCols + (Math.floor(crystal.x / map.tileWidth));

    const crystalIndices = [];
    for (let i = crystalHighestIndex; i <= crystalLowestIndex; i += map.numCols) {
        crystalIndices.push(i);
    }

    if (crystalIndices.indexOf(nextIndex) !== -1) {
        return true;
    }
}

export const mapAlignDownOffset = (charPosition, map) => {
    const mapHeight = map.tileHeight * map.numRows * map.scale;
    const visibleHeight = screenHeight > MAX_SCREEN_HEIGHT ? MAX_SCREEN_HEIGHT : screenHeight;
    const visibleEnd = visibleHeight - map.scrollOffset * map.scale;

    if (visibleEnd >= mapHeight) return 0;

    const nextStep = map.tileHeight * map.scale;
    const scaledCharPosition = charPosition * map.scale;
    const nextScaledPosition = scaledCharPosition + nextStep * 2;

    if (nextScaledPosition > visibleEnd) {
        return mapHeight - visibleEnd > nextStep ? nextStep : mapHeight - visibleEnd;
    }

    return 0;
}

export const mapAlignUpOffset = (charPosition, map) => {
    const visibleEnd = 0 - map.scrollOffset;

    if (visibleEnd <= 0) return 0;

    const nextStep = map.tileHeight * map.scale;
    const scaledCharPosition = charPosition * map.scale;
    const nextScaledPosition = scaledCharPosition - nextStep;

    if (nextScaledPosition < visibleEnd) {
        return visibleEnd < nextStep ? visibleEnd : nextStep;
    }

    return 0;
}

export const getData = (gid, tilesets) => {
    if (gid === 0) {
        return {
            indexOffset: 1,
            imageData: DataMap.imageData.TileSet_2,
            image: DataMap.images.TileSet_2,
            tileType: ''
        };
    }

    for (let i = 0; i < tilesets.length; i++) {
        let indexOffset, sourceArray;
        if (gid < tilesets[i].firstgid) {
            indexOffset = tilesets[i - 1].firstgid;
            sourceArray = tilesets[i - 1].source.split('/');
        } else if (i === tilesets.length - 1) {
            indexOffset = tilesets[i].firstgid;
            sourceArray = tilesets[i].source.split('/');
        }

        if (indexOffset && sourceArray) {
            const sourceString = sourceArray[sourceArray.length - 1].split('.')[0];
            const imageData = DataMap.imageData[`${sourceString}`];

            if (imageData.image) {
                const imageString = imageData.image.split('.')[0];
                const image = DataMap.images[`${imageString}`];

                const tileType = imageData.tileproperties ?
                    imageData.tileproperties[`${gid - indexOffset}`] ?
                        imageData.tileproperties[`${gid - indexOffset}`].Type : '' : '';

                return {
                    indexOffset,
                    imageData,
                    image,
                    tileType
                }
            } else {
                const data = imageData.tiles[`${gid - indexOffset}`];

                const imageStringArray = data.image.split('/');
                const imageString = imageStringArray[imageStringArray.length - 1].split('.')[0];
                const image = DataMap.images[`${imageString}`];

                return {
                    indexOffset: gid,
                    imageData: data,
                    image
                }
            }
        }
    }
}
