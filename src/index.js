import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css'

// Generate required css
import nyala from './assets/fonts/nyala.ttf';
const iconFontStyles = `@font-face {
  src: url(${nyala});
  font-family: Nyala;
}`;

// Create stylesheet
const style = document.createElement('style');
style.type = 'text/css';
if (style.styleSheet) {
    style.styleSheet.cssText = iconFontStyles;
} else {
    style.appendChild(document.createTextNode(iconFontStyles));
}

// Inject stylesheet
document.head.appendChild(style);

ReactDOM.render(<App />, document.getElementById('root'));
