import { REDUX_ACTIONS, DIRECTIONS } from '../constants';
import {
    isTilePassable,
    hasReachedCrytsal,
    isWordStone,
    mapAlignDownOffset,
    mapAlignUpOffset
} from '../utils';

export const setGameState = data => {
    return {
        type: REDUX_ACTIONS.SET_GAME_STATE,
        data
    }
}

export const loadPuzzle = data => {
    return {
        type: REDUX_ACTIONS.LOAD_PUZZLE,
        data
    }
}

export const moveBlott = (step, direction) => {
    return {
        type: direction,
        step
    }
}

export const moveBlottRight = step => {
    return {
        type: REDUX_ACTIONS.MOVE_RIGHT,
        step
    }
}

export const moveBlottLeft = step => {
    return {
        type: REDUX_ACTIONS.MOVE_LEFT,
        step
    }
}

export const moveBlottUp = step => {
    return {
        type: REDUX_ACTIONS.MOVE_UP,
        step
    }
}

export const moveBlottDown = step => {
    return {
        type: REDUX_ACTIONS.MOVE_DOWN,
        step
    }
}

export const removeTile = index => {
    return {
        type: REDUX_ACTIONS.REMOVE_TILE,
        index
    }
}

export const setPlaceHolderText = (id, text) => {
    return {
        type: REDUX_ACTIONS.SET_PLACEHOLDER_TEXT,
        id,
        text
    }
}

export const setAffixString = (id, string) => {
    return {
        type: REDUX_ACTIONS.SET_AFFIX_STRING,
        id,
        string
    }
}

export const setAnimatingFalse = () => {
    return {
        type: REDUX_ACTIONS.ANIMATING_FALSE
    }
}

export const setTileType = (index, tileType) => {
    return {
        type: REDUX_ACTIONS.SET_TILE_TYPE,
        index,
        tileType
    }
}

export const setMapScale = scale => {
    return {
        type: REDUX_ACTIONS.SET_SCALE,
        scale
    }
}

export const setModalClose = () => {
    return {
        type: REDUX_ACTIONS.SET_MODAL_CLOSE
    }
}

export const setPlaceHolderMeasure = (id, measurement) => {
    return {
        type: REDUX_ACTIONS.SET_PLACEHOLDER_MEASURE,
        id,
        measurement
    }
}

export const uncorruptTiles = () => {
    return {
        type: REDUX_ACTIONS.UNCORRUPT_TILES
    }
}

export const corruptTiles = () => {
    return {
        type: REDUX_ACTIONS.CORRUPT_TILES
    }
}

export const setModalVisible = () => {
    return {
        type: REDUX_ACTIONS.SET_MODAL_VISIBLE
    }
}

export const setStoneTouchable = () => {
    return {
        type: REDUX_ACTIONS.SET_STONE_TOUCHABLE
    }
}

export const setStoneUntouchable = () => {
    return {
        type: REDUX_ACTIONS.SET_STONE_UNTOUCHABLE
    }
}

export const setCrystalCollected = () => {
    return {
        type: REDUX_ACTIONS.SET_CRYSTAL_COLLECTED
    }
}

export const alignVisibleScreenDown = offset => {
    return {
        type: REDUX_ACTIONS.ALIGN_MAP_DOWN,
        offset
    }
}

export const alignVisibleScreenUp = offset => {
    return {
        type: REDUX_ACTIONS.ALIGN_MAP_UP,
        offset
    }
}

export const setCrystalPlaceholder = position => {
    return {
        type: REDUX_ACTIONS.SET_CRYSTAL_PLACEHOLDER,
        position
    }
}

export const checkPuzzleSolved = () => {
    return (dispatch, getState) => {
        const puzzleWord = getState().puzzleWord.filter(part => part.type === 'affix');

        let isSolved = true;

        puzzleWord.forEach(part => {
            if (part.text !== part.answer) {
                isSolved = false;
            }
        })

        if (isSolved) {
            dispatch(uncorruptTiles());
        } else {
            dispatch(corruptTiles());
        }
    }
}

export const move = direction => {
    return (dispatch, getState) => {
        const blott = getState().blott;
        const map = getState().mapData;
        const tiles = getState().tiles;
        const isModalVisible = getState().modalVisible;
        const puzzleTiles = getState().puzzleTiles;

        let currentHead, mapLength, xPos, yPos, nextIndex, movePossible = false;
        switch (direction) {
            case DIRECTIONS.RIGHT:
                currentHead = blott.x + blott.width / 2 + blott.nextXMove;
                mapLength = map.tileWidth * map.numCols;

                xPos = blott.x + blott.nextXMove + map.tileWidth;
                yPos = blott.y + blott.nextYMove;
                nextIndex = (Math.floor(yPos / map.tileHeight) - 1) * map.numCols + (Math.floor(xPos / map.tileWidth) + 1);

                movePossible = currentHead + map.tileWidth < mapLength;
                break;

            case DIRECTIONS.LEFT:
                currentHead = blott.x + blott.width / 2 + blott.nextXMove;

                xPos = blott.x + blott.nextXMove - map.tileWidth;
                yPos = blott.y + blott.nextYMove;
                nextIndex = (Math.floor(yPos / map.tileHeight) - 1) * map.numCols + (Math.floor(xPos / map.tileWidth) + 1);

                movePossible = currentHead - map.tileWidth > 0;
                break;

            case DIRECTIONS.DOWN:
                currentHead = blott.y + blott.nextYMove - blott.width / 2;
                mapLength = map.tileHeight * map.numRows;

                yPos = blott.y + blott.nextYMove + map.tileHeight;
                xPos = blott.x + blott.nextXMove;
                nextIndex = (Math.floor(yPos / map.tileHeight) - 1) * map.numCols + (Math.floor(xPos / map.tileWidth) + 1);

                movePossible = currentHead + map.tileHeight < mapLength;
                dispatch(alignVisibleScreenDown(mapAlignDownOffset(yPos - map.tileHeight, map)));
                break;

            case DIRECTIONS.UP:
                currentHead = blott.y + blott.nextYMove - blott.width / 2;

                yPos = blott.y + blott.nextYMove - map.tileHeight;
                xPos = blott.x + blott.nextXMove;
                nextIndex = (Math.floor(yPos / map.tileHeight) - 1) * map.numCols + (Math.floor(xPos / map.tileWidth) + 1);

                movePossible = currentHead - map.tileHeight > 0;
                dispatch(alignVisibleScreenUp(mapAlignUpOffset(yPos + map.tileHeight - blott.width, map)));
                break;

            default:
                break;
        }

        if (movePossible && isTilePassable(nextIndex, tiles, puzzleTiles) && !blott.isAnimating && !isModalVisible) {
            dispatch([moveBlott(map.tileWidth, direction), removeTile(nextIndex)]);
            if (isWordStone(nextIndex, tiles, map.numCols)) {
                dispatch(setStoneTouchable())
            } else {
                dispatch(setStoneUntouchable())
            }

            if (!getState().crystalCollected) {
                if (hasReachedCrytsal(getState().crystal, nextIndex, map)) {
                    dispatch(setCrystalCollected());
                }
            }
        }
    }
}
