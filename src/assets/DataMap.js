/* eslint-disable global-require */

const DataMap = {
    images: {
        TileSet_2: require('./images/TileSet_2.png'),
        background: require('./images/background.png'),
        base: require('./images/base.png'),
        Crystal1: require('./images/Crystal1.png'),
        Door: require('./images/Door.png'),
        Layer1: require('./images/Layer1.png'),
        Layer3: require('./images/Layer3.png'),
        PuzzleSentence_TileSet: require('./images/PuzzleSentence_TileSet.png'),
        Walking: require('./images/Walking.png'),
        backgroundTile: require('./images/PathTile_Sample.jpg'),
        BG: require('./images/BG.png'),
        Blott: require('./images/Blott.png'),
        DoneButton: require('./images/DoneButton.png'),
        HelpButton: require('./images/HelpButton.png'),
        InfoStone: require('./images/InfoStone.png'),
        InnerWordsBG_long: require('./images/InnerWordsBG_long.png'),
        InnerWordsBG_short: require('./images/InnerWordsBG_short.png'),
        UpperAffixBG: require('./images/UpperAffixBG.png')
    },
    imageData: {
        TileSet_2: require('./images/TileSet_2.json'),
        PuzzleSentence_TileSet: require('./images/PuzzleSentence_TileSet.json'),
        Images: require('./images/Images.json')
    }
}

export default DataMap;