import { REDUX_ACTIONS } from '../constants';

const puzzleWord = (state = [], action) => {
    switch (action.type) {
        case REDUX_ACTIONS.LOAD_PUZZLE:
            return action.data.puzzleWord.map(part => {
                return { ...part, id: Math.random(), measurement: null }
            });
        case REDUX_ACTIONS.SET_PLACEHOLDER_MEASURE:
            return state.map(part =>
                part.id === action.id && part.measurement === null ?
                    { ...part, measurement: action.measurement } :
                    part
            );
        case REDUX_ACTIONS.SET_PLACEHOLDER_TEXT:
            return state.map(part =>
                part.id === action.id ?
                    { ...part, text: action.text } :
                    part
            );
        default:
            return state;
    }
}

export default puzzleWord;