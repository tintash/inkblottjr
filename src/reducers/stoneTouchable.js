import { REDUX_ACTIONS } from '../constants';

const stoneTouchable = (state = false, action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_STONE_TOUCHABLE:
            return true;
        case REDUX_ACTIONS.SET_STONE_UNTOUCHABLE:
            return false;
        default:
            return state;
    }
}

export default stoneTouchable;