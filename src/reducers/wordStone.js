import { REDUX_ACTIONS } from '../constants';
import { getData } from '../utils';

const wordStone = (state = [], action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_GAME_STATE:
            return setStoneState(action.data.layers, action.data.tilesets);
        default:
            return state;
    }
}

const setStoneState = (layers, tilesets) => {
    const stonesOnly = layers.filter(layer => layer.name === 'WordStone')[0].objects;
    return stonesOnly.map(obj => {
        const data = getData(obj.gid, tilesets);
        return {
            ...obj,
            type: 'wordstone',
            image: data.image,
            data: data.imageData,
            indexOffset: data.indexOffset,
            name: stonesOnly.name
        }
    })
}

export default wordStone;