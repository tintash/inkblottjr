import { REDUX_ACTIONS } from '../constants';

const crystalCollected = (state = false, action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_CRYSTAL_COLLECTED:
            return true;
        default:
            return state;
    }
}

export default crystalCollected;