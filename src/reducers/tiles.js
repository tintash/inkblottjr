import { REDUX_ACTIONS } from '../constants';
import { getData } from '../utils';

const tiles = (state = [], action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_GAME_STATE:
            return setTileState(action.data.layers, action.data.tilesets);
        case REDUX_ACTIONS.REMOVE_TILE:
            if (state[0][action.index].tileIndex === 0) return state;
            let layer = state[0].map((tile, tileIndex) =>
                tileIndex === action.index ?
                    { ...tile, tileIndex: 0 } :
                    tile)
            return [layer, ...state.slice(1)];
        case REDUX_ACTIONS.SET_TILE_TYPE:
            layer = state[0].map((tile, tileIndex) =>
                tileIndex === action.index ?
                    { ...tile, tileType: action.tileType } :
                    tile)
            return [layer, ...state.slice(1)];
        case REDUX_ACTIONS.UNCORRUPT_TILES:
            layer = state[0].map(tile =>
                tile.tileType.includes('Corrupt') && tile.tileIndex !== 0 ?
                    { ...tile, oldIndex: tile.tileIndex, tileIndex: 51, tileType: 'Converted' } :
                    tile)
            return [layer, ...state.slice(1)];
        case REDUX_ACTIONS.CORRUPT_TILES:
            layer = state[0].map(tile =>
                tile.tileType.includes('Converted') && tile.tileIndex !== 0 ?
                { ...tile, tileIndex: tile.oldIndex, tileType: 'CorruptTile'} :
                tile)
            return [layer, ...state.slice(1)];
        default:
            return state;
    }
}

const setTileState = (layers, tilesets) => {
    const tiles = [];
    layers.forEach((layer, index) => {
        if (layer.name !== 'Blott_Char' && layer.name !== 'UI'
            && layer.name !== 'affix' && layer.name !== 'Crystal'
            && layer.name !== 'WordStone' && layer.name !== 'PuzzleSentence') {
            if (layer.data) {
                tiles.push(layer.data.map(tileIndex => {
                    const data = getData(tileIndex, tilesets);
                    return {
                        type: 'tile',
                        tileIndex: tileIndex,
                        image: data.image,
                        imageData: data.imageData,
                        indexOffset: data.indexOffset,
                        tileType: data.tileType,
                        name: layer.name
                    }
                }))
            } else {
                const decos = layer.objects.filter(obj => obj.gid !== undefined);
                if (decos.length)
                    tiles.push(decos.map(obj => {
                        const data = getData(obj.gid, tilesets);
                        return {
                            ...obj,
                            type: 'decoration',
                            image: data.image,
                            data: data.imageData,
                            indexOffset: data.indexOffset,
                            name: layer.name
                        }
                    })
                    );
            }
        }
    })
    return tiles;
}

export default tiles;