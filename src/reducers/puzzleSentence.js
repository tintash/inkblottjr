import { REDUX_ACTIONS } from '../constants';

const puzzleSentence = (state = '', action) => {
    switch (action.type) {
        case REDUX_ACTIONS.LOAD_PUZZLE:
            return action.data.puzzleSentence;
        default:
            return state;
    }
}

export default puzzleSentence;