import { REDUX_ACTIONS } from '../constants';

const crystalPlaceholder = (state = null, action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_CRYSTAL_PLACEHOLDER:
            return action.position;
        default:
            return state;
    }
}

export default crystalPlaceholder;