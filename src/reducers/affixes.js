import { REDUX_ACTIONS } from '../constants';
import { getData } from '../utils';

const affixes = (state = [], action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_GAME_STATE:
            return setAffixState(action.data.layers, action.data.tilesets);
        case REDUX_ACTIONS.LOAD_PUZZLE:
            return state.map((affix, index) => {
                return { ...affix, string: action.data.affixes[index] }
            });
        case REDUX_ACTIONS.SET_AFFIX_STRING:
            return state.map(affix =>
                affix.id === action.id ?
                { ...affix, string: action.string } :
                affix
            );
        default:
            return state;
    }
}

const setAffixState = (layers, tilesets) => {
    const affOnly = layers.filter(layer => layer.name === 'affix')[0].objects;
    return affOnly.map(obj => {
        const data = getData(obj.gid, tilesets);
        return {
            ...obj,
            type: 'affix',
            id: Math.random(),
            image: data.image,
            data: data.imageData,
            indexOffset: data.indexOffset,
            name: affOnly.name,
            string: ''
        }
    })
}

export default affixes;