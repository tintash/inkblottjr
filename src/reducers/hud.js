import { REDUX_ACTIONS } from '../constants';
import { getData } from '../utils';

const hud = (state = [], action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_GAME_STATE:
            return setHUDState(action.data.layers, action.data.tilesets);
        default:
            return state;
    }
}

const setHUDState = (layers, tilesets) => {
    const hudOnly = layers.filter(layer => layer.name === 'UI')[0].objects;
    return hudOnly.map(obj => {
        const data = getData(obj.gid, tilesets);
        return {
            ...obj,
            type: 'hud',
            image: data.image,
            data: data.imageData,
            indexOffset: data.indexOffset,
            name: hudOnly.name
        }
    })
}
export default hud;