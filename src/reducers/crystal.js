import DataMap from '../assets/DataMap';
import { REDUX_ACTIONS } from '../constants';
import { getData } from '../utils';

const initialState = {
    type: 'crystal',
    image: DataMap.images.Crystal1,
    data: '',
    indexOffset: 0,
    name: 'crystal'
}

const crystal = (state = initialState, action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_GAME_STATE:
            return setCrystalState(action.data.layers, action.data.tilesets);
        default:
            return state;
    }
}

const setCrystalState = (layers, tilesets) => {
    const crystalOnly = layers.filter(layer => layer.name === 'Crystal')[0].objects[0];
    const data = getData(crystalOnly.gid, tilesets);

    return {
        ...crystalOnly,
        type: 'crystal',
        image: data.image,
        data: data.imageData,
        indexOffset: data.indexOffset,
        name: 'crystal'
    }
}

export default crystal;