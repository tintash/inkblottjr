import { REDUX_ACTIONS } from '../constants';
import { getData } from '../utils';

const puzzleTiles = (state = [], action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_GAME_STATE:
            return setTilesState(action.data.layers, action.data.tilesets);
        default:
            return state;
    }
}

const setTilesState = (layers, tilesets) => {
    const layerData = layers.filter(layer => layer.name === 'PuzzleSentence')[0].data;
    return layerData.map(tileIndex => {
        const data = getData(tileIndex, tilesets);
        return {
            type: 'tile',
            tileIndex: tileIndex,
            image: data.image,
            imageData: data.imageData,
            indexOffset: data.indexOffset,
            tileType: data.tileType,
            name: 'PuzzleSentence'
        }
    })
}

export default puzzleTiles;