import { REDUX_ACTIONS } from '../constants';

const mapData = (state = {}, action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_GAME_STATE:
            return { numRows: action.data.height, numCols: action.data.width, tileHeight: action.data.tileheight, tileWidth: action.data.tilewidth, scrollOffset: 0, scale: 1 }
        case REDUX_ACTIONS.SET_SCALE:
            return { ...state, scale: action.scale }
        case REDUX_ACTIONS.ALIGN_MAP_DOWN:
            if(action.offset > 0)
                return { ...state, scrollOffset: state.scrollOffset - action.offset }
            return state;
        case REDUX_ACTIONS.ALIGN_MAP_UP:
            if (action.offset > 0)
                return { ...state, scrollOffset: state.scrollOffset + action.offset }
            return state;
        default:
            return state;
    }
}

export default mapData;