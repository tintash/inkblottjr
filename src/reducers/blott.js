import DataMap from '../assets/DataMap';
import { DIRECTIONS, REDUX_ACTIONS } from '../constants';
import { getData } from '../utils';

const initialState = {
    x: 0,
    y: 0,
    type: 'character',
    image: DataMap.images.Walking,
    data: '',
    indexOffset: 0,
    nextXMove: 0,
    nextYMove: 0,
    xFacing: 1,
    yFacing: '0deg',
    isHorizontal: true,
    isAnimating: false
}

const blott = (state = initialState, action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_GAME_STATE:
            return setBlottState(action.data.layers, action.data.tilesets);
        case DIRECTIONS.RIGHT:
            return { ...state, nextXMove: state.nextXMove + action.step, xFacing: 1, yFacing: '0deg', isHorizontal: true, isAnimating: true }
        case DIRECTIONS.LEFT:
            return { ...state, nextXMove: state.nextXMove - action.step, xFacing: -1, yFacing: '0deg', isHorizontal: true, isAnimating: true }
        case DIRECTIONS.DOWN:
            let xFacing = state.xFacing;
            if(state.yFacing === '-90deg') xFacing = state.xFacing * -1;

            return { ...state, nextYMove: state.nextYMove + action.step, yFacing: '90deg', xFacing: xFacing, isHorizontal: false, isAnimating: true };
        case DIRECTIONS.UP:
            xFacing = state.xFacing;
            if (state.yFacing === '90deg') xFacing = state.xFacing * -1;

            return { ...state, nextYMove: state.nextYMove - action.step, yFacing: '-90deg', xFacing: xFacing, isHorizontal: false, isAnimating: true }
        case REDUX_ACTIONS.ANIMATING_FALSE:
            return { ...state, isAnimating: false }
        default:
            return state;
    }
}

const setBlottState = (layers, tilesets) => {
    const blottOnly = layers.filter(layer => layer.name === 'Blott_Char');
    if (blottOnly.length) {
        const blott = blottOnly[0].objects[0];
        const data = getData(blott.gid, tilesets);
        return {
            ...blott,
            x: blott.x - blott.width/5,
            type: 'character',
            image: data.image,
            data: data.imageData,
            indexOffset: data.indexOffset,
            nextXMove: 0,
            nextYMove: 0,
            xFacing: 1,
            yFacing: '0deg',
            isHorizontal: true,
            isAnimating: false
        }
    }

    return null;
}

export default blott;