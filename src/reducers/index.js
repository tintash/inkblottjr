import { combineReducers } from 'redux';

import tiles from './tiles';
import mapData from './mapData';
import blott from './blott';
import modalVisible from './modalVisible';
import stoneTouchable from './stoneTouchable';
import hud from './hud';
import affixes from './affixes';
import crystal from './crystal';
import crystalCollected from './crystalCollected';
import wordStone from './wordStone';
import puzzleSentence from './puzzleSentence';
import puzzleWord from './puzzleWord';
import puzzleTiles from './puzzleTiles';
import crystalPlaceholder from './crystalPlaceholder';

const rootReducer = combineReducers({
    tiles,
    mapData,
    blott,
    modalVisible,
    stoneTouchable,
    hud,
    affixes,
    crystal,
    crystalCollected,
    wordStone,
    puzzleSentence,
    puzzleWord,
    puzzleTiles,
    crystalPlaceholder
})

export default rootReducer;