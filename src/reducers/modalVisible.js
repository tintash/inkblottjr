import { REDUX_ACTIONS } from '../constants';

const modalVisible = (state = false, action) => {
    switch (action.type) {
        case REDUX_ACTIONS.SET_MODAL_VISIBLE:
            return true;
        case REDUX_ACTIONS.SET_MODAL_CLOSE:
            return false;
        default:
            return state;
    }
}

export default modalVisible;