import React from 'react'
import { Provider } from 'react-redux'
import { View, Text, StyleSheet, StatusBar } from 'react-native'
import { Scene, Router } from 'react-native-router-flux';
import { createStore, applyMiddleware, compose } from 'redux'
import { Font } from 'expo';
import thunk from 'redux-thunk';
import { reduxBatch } from '@manaflair/redux-batch';
import rootReducer from './src/reducers';
import HomeScreen from './src/components/HomeScreen';
import BlottActivity from './src/components/BlottActivity';

const store = createStore(
  rootReducer,
  compose(reduxBatch, applyMiddleware(thunk), reduxBatch)
);

class App extends React.Component {
  componentWillMount() {
    StatusBar.setHidden(true);
  }

  componentDidMount() {
    Font.loadAsync({
      'nyala': require('./src/assets/fonts/nyala.ttf')
    })
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <Scene key='root' hideNavBar>
            <Scene
              key='Home'
              component={HomeScreen}
              type='replace'
              initial />
            <Scene
              key='Game'
              component={BlottActivity}
              type='replace' />
          </Scene>
        </Router>
      </Provider>
    )
  }
}

export default App;