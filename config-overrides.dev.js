const path = require('path');

module.exports = function (config) {
    let loaderList = config.module.rules[1].oneOf;
    loaderList.splice(loaderList.length - 1, 0, {
        test: /\.js$/,
        exclude: /node_modules\/(?!(react-native-elements|react-native-modalbox|react-native-swipe-gestures|react-native-web-lists|react-native-easy-grid|react-native-vector-icons|react-native-modal|react-native-animatable|react-navigation|react-native-router-flux|react-native-experimental-navigation|react-native-tabs|react-native-responsive)\/).*/,
        loader: 'babel-loader',
    });

    loaderList.splice(loaderList.length - 1, 0, {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: 'file-loader',
    });

    aliasList = config.resolve.alias;
    aliasList['FlatList'] = 'react-native-web-lists/src/FlatList';
}