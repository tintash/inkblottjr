# Aqua React Native Game

## About

Sample puzzle game built in React Native, incorporating React-Native-Web. Built upon the boilerplate.

## 'Src' Directory Structure

```
├── actions/
| └── index.js
├── assets/
| ├── images/
| ├── fonts/
| ├── DataMap.js
| ├── DemoLevel.json
| └── DemoPuzzle.json
├── components/
| ├── Affixes/
| | ├── Affix.js
| | └── AffixLayer.js
| ├── Blott/
| | ├── Character.js
| | └── CharacterMovement.js
| ├── Decorations/
| | ├── Crystal.js
| | └── MapDecorationLayer.js
| ├── HUD/
| | ├── AffixPlaceHolder.js
| | ├── CrystalCollector.js
| | ├── Hudlayer.js
| | └── PauseButton.js
| ├── Map/
| | ├── Background.js
| | ├── MapLayer.js
| | ├── MapTile.js
| | ├── PuzzleMapLayer.js
| | └── Tile.js
| ├── Popup/
| | ├── ModalButton.js
| | ├── ModalImage.js
| | ├── ModalInterface.js
| | ├── ModalLayer.js
| | ├── Puzzle.js
| | └── WordModal.js
| ├── BlottActivity.js
| ├── GameScreen.js
| ├── HomeScreen.js
| ├── MapImage.js
| └── Styles.js
├── reducers/
| ├── index.js
| ├── affixes.js
| ├── blott.js
| ├── crystal.js
| ├── crystalCollected.js
| ├── crystalPlaceholder.js
| ├── hud.js
| ├── mapData.js
| ├── modalVisible.js
| ├── puzzleSentence.js
| ├── puzzleTiles.js
| ├── puzzleWord.js
| ├── stoneTouchable.js
| ├── tiles.js
| └── wordStone.js
├── utils/
| └── index.js
├── App.js
├── constants.js
├── index.css
└── index.js
```

## Setting up

Clone repo and install packages:
```
$ npm install
```
For running the project on specific platforms, see below.

#### Running for Web
```
$ npm start
```

#### Running for Android
Start your emulator and run:
```
$ npm run android
```

#### Running for iOS:
Make sure Xcode is running and run:
```
$ npm run ios
```
For more commands, check out `package.json`.

## Notes

-   App.js in the root folder is the entry point for the Android and iOS apps. The entry point for the web app is src -> App.js.
-   To make webpack modifications, use config-overrides.dev.js and config-overrides.prod.js.